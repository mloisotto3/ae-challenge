import * as React from 'react'
import {
  TouchableOpacity,
  Image,
} from 'react-native'
import styles from '../styles'

type Props = {
  imageUrl: string,
  imageId: number,
  openPicture: Function,
  imageStyle: Object,
}

class ListItem extends React.PureComponent<Props> {
  handleOnPress = () => this.props.openPicture(this.props.imageId);

  render () {
    const { imageUrl, imageStyle } = this.props
    return (
      <TouchableOpacity
        onPress={this.handleOnPress}
        style={styles.item}
      >
        <Image
          style={imageStyle}
          resizeMode='cover'
          source={{ uri: imageUrl }}
        />
      </TouchableOpacity>
    )
  }
}

export default ListItem
