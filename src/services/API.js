// @flow
import axios from 'axios';
import CONSTANTS from '../constants';
import { AsyncStorage } from 'react-native';

export async function authenticate(): Object {
  try {
    const response = await axios.post(`${CONSTANTS.API_ENDPOINT}/auth`, {
      apiKey: CONSTANTS.API_KEY,
    });
    if (response.status === 200) {
      return response.data.token;
    }

    return { status: response.status, error: response.body };
  } catch (e) {
    throw e;
  }
}

export async function getPictures(page: number = 1): Array<Object> {
  try {
    const token = await AsyncStorage.getItem('auth_token');

    if (!token) {
      throw new Error('User is not authenticated!');
    }

    const response = await axios.get(
      `${CONSTANTS.API_ENDPOINT}/images?page=${page}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    if (response.status === 200) {
      return response.data;
    }

    return { status: response.status, error: response.body };
  } catch (e) {
    throw e;
  }
}

export async function getPictureDetails(id: number): Object {
  try {
    const token = await AsyncStorage.getItem('auth_token');

    if (!token) {
      throw new Error('User is not authenticated!');
    }

    const response = await axios.get(`${CONSTANTS.API_ENDPOINT}/images/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (response.status === 200) {
      return response.data;
    }

    return { status: response.status, error: response.body };
  } catch (e) {
    throw e;
  }
}
