// @flow
import * as React from 'react'
import { View, Animated, Text } from 'react-native'
import { PinchGestureHandler, State } from 'react-native-gesture-handler'

import styles from './styles'
import DetailsFooter from './components/DetailsFooter'

type Props = {
  imageUrl: string,
  shareCallback: Function,
  applyFilterCallback: Function,
  pictureDetails: Object,
}

// TODO: it would be great to see here loader, pinch to zoom here and pan

class DetailView extends React.PureComponent<Props> {
  scale = new Animated.Value(1);

  onZoomEvent = Animated.event(
    [
      {
        nativeEvent: { scale: this.scale },
      },
    ],
    {
      useNativeDriver: true,
    }
  );

  onZoomStateChange = (event) => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      Animated.spring(this.scale, {
        toValue: 1,
        useNativeDriver: true,
      }).start()
    }
  };

  render () {
    const {
      imageUrl,
      shareCallback,
      applyFilterCallback,
      pictureDetails,
    } = this.props

    const uri = pictureDetails ? pictureDetails.full_picture : imageUrl

    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          {!uri ? <Text>Loading...</Text> : <PinchGestureHandler
            onGestureEvent={this.onZoomEvent}
            onHandlerStateChange={this.onZoomStateChange}
          >
            <Animated.Image
              source={{uri}}
              style={{
                width: '100%',
                height: 300,
                transform: [{ scale: this.scale }],
              }}
              resizeMode='contain'
            />
          </PinchGestureHandler>}
        </View>
        <DetailsFooter
          pictureDetails={pictureDetails}
          shareCallback={shareCallback}
          applyFilterCallback={applyFilterCallback}
        />
      </View>
    )
  }
}

export default DetailView
