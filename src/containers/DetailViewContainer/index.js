// @flow
import * as React from 'react'
import { Share } from 'react-native'
import DetailView from '../../screens/DetailView'
import { connect } from 'react-redux'
import { fetchPictureDetails } from './actions'
import { selectHiResImage } from './selectors'

export interface Props {
  navigation: any,
  fetchPictureDetails: Function,
  isLoading: boolean,
  hiResImage: Function,
}
export interface State {
  imageUrl: string,
}

class DetailViewContainer extends React.Component<Props, State> {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: 'transparent',
      position: 'absolute',
      height: 50,
      top: 0,
      left: 0,
      right: 0,
      borderBottomWidth: 0,
      elevation: 0,
    },
    headerTintColor: '#FFF',
  }

  componentDidMount () {
    const { navigation, fetchPictureDetails } = this.props
    const { pictureDetails } = navigation.state.params
    if (!this.props.hiResImage(pictureDetails.id)) {
      fetchPictureDetails(pictureDetails.id)
    }
  }

  share = (imageId: number): void => {
    const message = ''
    const url = ''

    Share.share(
      {
        message,
        url,
        title: 'Take a look at this picture!',
      },
      {
        // Android only:
        dialogTitle: 'Share a photo',
      },
    )
  };

  applyFilter = (type): void => {
    // TODO: implement apply image filter function
  }

  render () {
    const { pictureDetails } = this.props.navigation.state.params
    const imageURL = pictureDetails.cropped_picture
    const { hiResImage } = this.props

    const pic = hiResImage(pictureDetails.id)

    return <DetailView
      imageUrl={imageURL}
      pictureDetails={pic || pictureDetails}
      shareCallback={this.share}
      applyFilterCallback={this.applyFilter}
    />
  }
}

function mapDispatchToProps (dispatch) {
  return {
    fetchPictureDetails: (imageId, token) => dispatch(fetchPictureDetails(imageId, token)),
  }
}

const mapStateToProps = state => ({
  hiResImage: imageId => selectHiResImage(state, imageId),
  isLoading: state.detailViewReducer.isLoading,
})

export default connect(mapStateToProps, mapDispatchToProps)(DetailViewContainer)
