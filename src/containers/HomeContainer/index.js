// @flow
import * as React from 'react'
import { Platform, StatusBar, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'

import HomeView from '../../screens/Home'
import { fetchPictures } from './actions'
import {authenticate} from '../../services/API'

export interface Props {
  navigation: any,
  fetchPictures: Function,
  pictures: Array<Object>,
  isLoading: boolean,
}

export interface State {}

class HomeContainer extends React.Component<Props, State> {
  static navigationOptions = {
    title: 'Gallery App',
    headerTintColor: 'white',
    headerStyle: { backgroundColor: 'indigo' },
  };

  constructor (props) {
    super(props)
    StatusBar.setBarStyle('light-content')
    Platform.OS === 'android' && StatusBar.setBackgroundColor('indigo')
    this.onRefresh = this.onRefresh.bind(this)
    this.onLoadNext = this.onLoadNext.bind(this)

    this.state = {
      token: null,
    }
  }

  componentDidMount () {
    authenticate()
      .then(token => {
        AsyncStorage.setItem('auth_token', token, this.onRefresh)
      })
      .catch(err => this.setState({ err }))
  }

  onRefresh (): void {
    this.props.fetchPictures(1)
  }

  onLoadNext (e): void {
    this.props.fetchPictures(this.props.page + 1)
  }

  render () {
    return <HomeView {...this.props}
      authToken={this.state.token}
      onRefresh={this.onRefresh}
      onLoadNext={this.onLoadNext} />
  }
}

function mapDispatchToProps (dispatch) {
  return {
    fetchPictures: (page, token) => dispatch(fetchPictures(page, token)),
  }
}

const mapStateToProps = state => ({
  pictures: state.homeReducer.pictures,
  page: state.homeReducer.page,
  isLoading: state.homeReducer.isLoading,
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)
