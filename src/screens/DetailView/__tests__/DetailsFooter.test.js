import React from 'react'
import DetailFooter from '../components/DetailsFooter'
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer'

const shareCallback = jest.fn()
const applyFilterCallback = jest.fn()
const pictureDetails = {
  author: 'Shimmering Historian',
  camera: 'Canon 1300',
  cropped_picture: 'http://195.39.233.28:8035/pictures/cropped/0002.jpg',
  full_picture: 'http://195.39.233.28:8035/pictures/full_size/0002.jpg',
  id: '97678c8030746f1ebc80',
}

it('renders correctly', () => {
  const tree = renderer
    .create(
      <DetailFooter
        shareCallback={shareCallback}
        applyFilterCallback={applyFilterCallback}
        pictureDetails={pictureDetails}
      />
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})
