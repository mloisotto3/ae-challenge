import { AppRegistry } from 'react-native'
import App from './src/App'

import 'react-native-gesture-handler'

global.XMLHttpRequest = global.originalXMLHttpRequest
  ? global.originalXMLHttpRequest
  : global.XMLHttpRequest
global.FormData = global.originalFormData
  ? global.originalFormData
  : global.FormData

AppRegistry.registerComponent('AE500pxProject', () => App)
