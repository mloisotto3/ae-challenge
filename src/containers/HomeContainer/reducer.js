// @flow
import {FETCH_FAILED, PICTURES_FETCH_REQUESTED, PICTURES_FETCH_SUCCESS} from './actions';

const initialState = {
  pictures: [],
  isLoading: true,
  page: 1,
  errorMessage: '',
};

export default function (state: any = initialState, action: Object) {
  const payload = action.payload;

  switch (action.type) {
    case PICTURES_FETCH_SUCCESS:
      if (action.payload.page <= state.page) {
        return {
          ...state,
          pictures: [...payload.pictures],
          isLoading: false,
          page: payload.page,
        };
      } else {
        return {
          ...state,
          pictures: [...state.pictures, ...payload.pictures],
          isLoading: false,
          page: payload.page,
        };
      }
    case FETCH_FAILED:
      return {...state, isLoading: false, errorMessage: payload.errorMessage};
    case PICTURES_FETCH_REQUESTED:
      return { ...state, isLoading: true };
    default:
      return {...state};
  }
}
