import * as React from 'react'
import {
  TouchableOpacity,
  Image,
  View,
  Text,
} from 'react-native'
import styles from '../styles'
import imageFiltersImage from './images/ImageFilters.png'
import shareImage from './images/ShareThis.png'

type Props = {
  shareCallback: Function,
  colorSwitchCallback: Function,
  pictureDetails: Object,
}

class DetailsFooter extends React.PureComponent<Props> {
  handleClickShare = () => this.props.shareCallback(this.props.pictureDetails.id);

  render () {
    const { applyFilterCallback, pictureDetails } = this.props
    if (!pictureDetails) return null
    return (
      <View style={styles.detailView}>
        <View style={styles.textContent}>
          <Text style={styles.authorText}>{pictureDetails.author}</Text>
          <Text style={styles.textDetails}>{pictureDetails.camera}</Text>
        </View>
        <TouchableOpacity
          style={{ marginRight: 10 }}
          onPress={applyFilterCallback}
        >
          <Image
            style={styles.detailViewImage}
            resizeMode='cover'
            source={imageFiltersImage}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleClickShare}>
          <Image
            style={styles.detailViewImage}
            resizeMode='cover'
            source={shareImage}
          />
        </TouchableOpacity>
      </View>
    )
  }
}

export default DetailsFooter
