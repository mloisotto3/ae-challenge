import { Dimensions, StyleSheet } from 'react-native'
const { width } = Dimensions.get('window')

const styles: any = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
    flexDirection: 'column',
  },
  imageContainer: {
    flex: 0.8,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    flex: 1,
    width,
    height: undefined,
  },
  backButton: {
    position: 'absolute',
    left: 5,
    top: 5,
  },
  spinner: {
    position: 'absolute',
  },
  detailView: {
    // position: 'absolute',
    flex: 0.2,
    bottom: 10,
    width: width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  detailViewImage: {
    width: 50,
    height: 50,
  },
  textDetails: {
    color: 'white',
    fontSize: 18,
  },
  authorText: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 20,
  },
  textContent: {
    flexDirection: 'column',
  },
})
export default styles
