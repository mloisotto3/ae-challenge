import { getPictures } from '../API'

test('the response should be user unauthorized', async () => {
  expect.assertions(1)
  try {
    const data = await getPictures()
  } catch (err) {
    expect(typeof err).toBe('object')
  }
})
